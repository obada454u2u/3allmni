package com.example.a3allmni.Adapters;

import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a3allmni.DataProviders.LongVowelDataProvider;
import com.example.a3allmni.LongVowelActivity;
import com.example.a3allmni.R;
import com.example.a3allmni.SingleLetterActivity;
import com.example.a3allmni.model.letter;

public class LongVowelRecylerViewAdapter extends RecyclerView.Adapter<LongVowelRecylerViewAdapter.LongVowelViewHolder> {

    private LongVowelDataProvider  mData;
    private ItemClickListener mClickListener;


    // data is passed into the constructor
    public LongVowelRecylerViewAdapter(LongVowelDataProvider data) {
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public LongVowelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_longvowel_item, parent, false);
        return new LongVowelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LongVowelViewHolder holder, int position) {
        holder.myTextView.setText(mData.getExamples()[position].getTitle());

    }



    // total number of cells
    @Override
    public int getItemCount() {
        return mData.getExamples().length;
    }


    // stores and recycles views as they are scrolled off screen



    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public class LongVowelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ImageView imageView;

        LongVowelViewHolder(View itemView) {
            super(itemView);

            myTextView = itemView.findViewById(R.id.long_textTV);
            imageView = itemView.findViewById(R.id.long_plaIV);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

            Log.d("ad", "onItemClick: ONITEMCLICK TTTTTT");

        }
    }
}