package com.example.a3allmni.DataProviders;

import com.example.a3allmni.R;
import com.example.a3allmni.model.Word;

public class LongVowelDataProvider {

    private  String tiitle;
    private Word[] examples;

    public LongVowelDataProvider(String tiitle, Word[] examples) {
        this.tiitle = tiitle;
        this.examples = examples;
    }

     public Word[] getExamples() {
        return examples;
    }


    public static final  LongVowelDataProvider a = new LongVowelDataProvider("أ",
            new Word[]{
                    new Word("حا", R.raw.haaa),  new Word("جا", R.raw.ga),  new Word("دا", R.raw.daa),
                    new Word("خا", R.raw.gggaaa),  new Word("را", R.raw.raaaa),  new Word("ذا", R.raw.thaaa),

    }); //,"","","","",""
  public static final  LongVowelDataProvider b= new LongVowelDataProvider("ي",new Word[]{
          new Word("حي", R.raw.heee),  new Word("جي", R.raw.gee),  new Word("دي", R.raw.dee),
          new Word("خي", R.raw.ggee),  new Word("ري", R.raw.re),  new Word("ذي", R.raw.the),
});
  public static final  LongVowelDataProvider c = new LongVowelDataProvider("و",new Word[]{
          new Word("حو", R.raw.hooo),  new Word("جو", R.raw.go),  new Word("دو", R.raw.doo),
          new Word("خو", R.raw.ggo),  new Word("رو", R.raw.ro),  new Word("ذو", R.raw.tho),

    });

    public static LongVowelDataProvider[] getDATA(){
        return DATA ;
    }
    public static final LongVowelDataProvider[] DATA={a,b,c};

}
