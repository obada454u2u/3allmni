package com.example.a3allmni.DataProviders;

import com.example.a3allmni.R;
import com.example.a3allmni.model.Word;
import com.example.a3allmni.model.letter;


import java.util.Arrays;

public class lettersProvider {

    public static letter[] getLetters() {
        return DATA;
    }

//منزل   كتب

    public static final letter FIRS_LETTER = new letter(0, "أ", R.drawable.a, R.raw.a,
            Arrays.asList(new Word("أسد", R.drawable.lino0), new Word("بطاطا", R.drawable.potato0)));  //

    public static final letter SECOND_LETTER = new letter(1, "ب", R.drawable.b, R.raw.b
            , Arrays.asList(new Word("بطة", R.drawable.duck1), new Word("أرنب", R.drawable.rabbit1))); //


    public static final letter THIRDD_LETTER = new letter(2, "ت", R.drawable.taa, R.raw.taa,
            Arrays.asList(new Word("تاج", R.drawable.taj2), new Word("مفتاح", R.drawable.key2))
    ); //

    public static final letter FOURTH_LETTER = new letter(3, "ث", R.drawable.tha, R.raw.tha,
            Arrays.asList(new Word("مثلث", R.drawable.triangle3), new Word("ثعلب", R.drawable.fox3))
    ); //

    public static final letter Fi_LETTER = new letter(4, "ج", R.drawable.gem, R.raw.gem,
            Arrays.asList(new Word("جمل", R.drawable.camel4), new Word("حج", R.drawable.maka4))
    ); //

    public static final letter SITH_LETTER = new letter(5, "ح", R.drawable.h7a, R.raw.h7a,
            Arrays.asList(new Word("حجارة", R.drawable.rock5), new Word("حج", R.drawable.maka4))
    ); //
    public static final letter SEvTH_LETTER = new letter(6, "خ", R.drawable.cha, R.raw.cha,
            Arrays.asList(new Word("خيار", R.drawable.cuc7), new Word("خبز", R.drawable.bread6))
    ); //
    public static final letter NINETH_LETTER = new letter(7, "د", R.drawable.dal, R.raw.tha,
            Arrays.asList(new Word("ديك", R.drawable.deack7), new Word("أسد", R.drawable.lino0))
    ); //
    public static final letter TENTH_LETTER = new letter(8, "ذ", R.drawable.thal, R.raw.thal,
            Arrays.asList(new Word("ذئب", R.drawable.wolf), new Word("ذرة", R.drawable.corn))
    ); //
    public static final letter ELVTH_LETTER = new letter(9, "ر", R.drawable.raa, R.raw.raa,
            Arrays.asList(new Word("حجارة", R.drawable.rock5), new Word("ذرة", R.drawable.corn))
    ); //
    public static final letter TWETH_LETTER = new letter(10, "ز", R.drawable.zen, R.raw.zen,
            Arrays.asList(new Word("خبز", R.drawable.bread6), new Word("زهرة", R.drawable.flower))
    ); //
    public static final letter THRETH_LETTER = new letter(11, "س", R.drawable.sen, R.raw.sen,
            Arrays.asList(new Word("أسد", R.drawable.lino0), new Word("ساعة", R.drawable.clock))
    ); //
    public static final letter FORTE_LETTER = new letter(12, "ش", R.drawable.shen, R.raw.shen,
            Arrays.asList(new Word("شمس", R.drawable.sun), new Word("حشرة", R.drawable.bugy))
    ); //
    public static final letter FIFTE_LETTER = new letter(13, "ص", R.drawable.sad, R.raw.sad,

            Arrays.asList(new Word("قفص", R.drawable.cage), new Word("صوص", R.drawable.chick))
    ); //
    public static final letter SIXTE_LETTER = new letter(14, "ض", R.drawable.daad, R.raw.daad,

            Arrays.asList(new Word("ضفدع", R.drawable.frog), new Word("ضابط", R.drawable.officer))
    ); //
    public static final letter SEVENT_LETTER = new letter(15, "ط", R.drawable.ttaa, R.raw.ttaa,
            Arrays.asList(new Word("طفل", R.drawable.baby), new Word("ضابط", R.drawable.officer))
    ); //


    public static final letter EITGHNT_LETTER = new letter(16, "ظ", R.drawable.thha, R.raw.thha,
            Arrays.asList(new Word("ظفر", R.drawable.nail), new Word("ظرف", R.drawable.envelope))
    ); //

    public static final letter NINETEENT_LETTER = new letter(17, "ع", R.drawable.ean, R.raw.ean,
            Arrays.asList(new Word("عين", R.drawable.eye), new Word("ثعلب", R.drawable.fox3))
    ); //

    public static final letter TWENTY_LETTER = new letter(18, "غ", R.drawable.gheen, R.raw.gheen,
            Arrays.asList(new Word("غار", R.drawable.cave), new Word("غزال", R.drawable.deer))
    ); //


    /*
     * أ، ب، ت، ث، ج، ح، خ، د، ذ، ر، ز، س، ش، ص، ض، ط، ظ، ع، غ، ف، ق، ك، ل، م، ن، هـ، و، ي
     * */

    public static final letter TWENTY_TWo_LETTER = new letter(20, "ف", R.drawable.faa, R.raw.faa,
            Arrays.asList(new Word("ضفدع", R.drawable.frog), new Word("ظرف", R.drawable.envelope))
    ); //


    public static final letter TWENTY_THREE_LETTER = new letter(21, "ق", R.drawable.qaf, R.raw.qaf,
            Arrays.asList(new Word("قمر", R.drawable.moon), new Word("قفص", R.drawable.cage))
    ); //


    public static final letter TWENTY_FOUR_LETTER = new letter(22, "ك", R.drawable.kaf, R.raw.kaf,
            Arrays.asList(new Word("كلب", R.drawable.dog), new Word("كتب", R.drawable.books))
    ); //


    public static final letter TWENTY_FIVE_LETTER = new letter(23, "ل", R.drawable.lam, R.raw.lam,
            Arrays.asList(new Word("جمل", R.drawable.camel4), new Word("منزل", R.drawable.house))
    ); //

    public static final letter TWENTY_SIX_LETTER = new letter(24, "م", R.drawable.mem, R.raw.mem,
            Arrays.asList(new Word("جمل", R.drawable.camel4), new Word("منزل", R.drawable.house))
    ); //

    public static final letter TWENasdaEN_LETTER = new letter(25, "ن", R.drawable.non, R.raw.non,
            Arrays.asList(new Word("أرنب", R.drawable.rabbit1), new Word("منزل", R.drawable.house))
    ); //


    public static final letter TWENTY_SIVEN_LETTER = new letter(26, "ه", R.drawable.ha, R.raw.ha,
            Arrays.asList(new Word("هرم", R.drawable.pyra), new Word("زهرة", R.drawable.flower))
    ); //


    public static final letter TWENTY_EIGTH_LETTER = new letter(27, "و", R.drawable.waw, R.raw.waw,
            Arrays.asList(new Word("وجه", R.drawable.face), new Word("صوص", R.drawable.chick))
    ); //


    public static final letter TWENTY_NINE_LETTER = new letter(28, "ي", R.drawable.yaa, R.raw.yaa,
            Arrays.asList(new Word("خيار", R.drawable.deack7), new Word("يد", R.drawable.hand))
    ); //


    public static final letter[] DATA = {FIRS_LETTER, SECOND_LETTER, THIRDD_LETTER, FOURTH_LETTER, Fi_LETTER, SITH_LETTER, SEvTH_LETTER, NINETH_LETTER, TENTH_LETTER
            , ELVTH_LETTER, TWETH_LETTER, THRETH_LETTER, FORTE_LETTER, FIFTE_LETTER, SIXTE_LETTER, SEVENT_LETTER, EITGHNT_LETTER, NINETEENT_LETTER, TWENTY_LETTER,
            TWENTY_TWo_LETTER, TWENTY_THREE_LETTER, TWENTY_FOUR_LETTER, TWENTY_FIVE_LETTER, TWENTY_SIX_LETTER, TWENasdaEN_LETTER, TWENTY_SIVEN_LETTER, TWENTY_EIGTH_LETTER, TWENTY_NINE_LETTER};

}