package com.example.a3allmni;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.a3allmni.Adapters.DrawLetterRecyclerViewAdapter;
import com.example.a3allmni.Adapters.MyRecyclerViewAdapter;
import com.example.a3allmni.DataProviders.DrawLetterDataProvider;
import com.example.a3allmni.model.Word;
import com.example.a3allmni.model.letter;

public class DrawLetterChooser extends AppCompatActivity implements DrawLetterRecyclerViewAdapter.ItemClickListener{
    DrawLetterRecyclerViewAdapter adapter;
    Word[] DATA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_letter_chooser);
        DATA= DrawLetterDataProvider.getWords();
        RecyclerView recyclerView = findViewById(R.id.draw_letter_id_rv);
        int numberOfColumns = 3;
        GridLayoutManager myGridlayoutManager;
        myGridlayoutManager= new GridLayoutManager(this, numberOfColumns);
        //myGridlayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(myGridlayoutManager);
        adapter = new DrawLetterRecyclerViewAdapter( DATA);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(View view, int position) {

        Intent i = new Intent(DrawLetterChooser.this,SingleLetterDrawActivity.class);
        i.putExtra("IMAGE_RES",DATA[position].getImage());
        startActivity(i);

    }
}
