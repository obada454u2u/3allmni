package com.example.a3allmni.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.a3allmni.DrawLetterChooser;
import com.example.a3allmni.LettersActivity;
import com.example.a3allmni.LongVowelChooserActivity;
import com.example.a3allmni.R;
import com.example.a3allmni.ShortVowelActivity;
import com.example.a3allmni.SingleLetterDrawActivity;


public class Tab1Fragment extends Fragment {
    private static final String TAG = "Tab1Fragment";

Button fbtn,svbtn,thirbtn,fourt;
ImageView iv;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab1_fragment,container,false);

        iv = view.findViewById(R.id.fbtn);
        svbtn=view.findViewById(R.id.shortVowelId);
        thirbtn=view.findViewById(R.id.longVoeldId);
        fourt=view.findViewById(R.id.letter_draw_btn);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LettersActivity.class));
            }
        });
        svbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), ShortVowelActivity.class));
            }
        });

        thirbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), LongVowelChooserActivity.class));
            }
        });

        fourt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), DrawLetterChooser.class));
            }
        });


        return view;
    }
}
