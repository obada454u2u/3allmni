package com.example.a3allmni.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.a3allmni.Practices.FifthLevelPracticeActivity;
import com.example.a3allmni.Practices.FirstLevelPracticeActivity;
import com.example.a3allmni.Practices.FourthLevelPracticeActvity;
import com.example.a3allmni.Practices.SecondLevelPracticeActivity;
import com.example.a3allmni.Practices.ThirdLevelPracticeActivity;
import com.example.a3allmni.R;

public class Tab2Fragment extends Fragment {
    private static final String TAG = "Tab1Fragment";

    private TextView tv1,tv2,tv3,tv4,tv5;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab2_fragment,container,false);
        tv1 = view.findViewById(R.id.practice_1);
        tv2=view.findViewById(R.id.practice_2);
        tv3=view.findViewById(R.id.practice_3);
        tv4=view.findViewById(R.id.practice_4);
        tv5=view.findViewById(R.id.practice_5);

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), FirstLevelPracticeActivity.class));
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SecondLevelPracticeActivity.class));
            }
        });

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), ThirdLevelPracticeActivity.class));
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), FourthLevelPracticeActvity.class));
            }
        });

        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), FifthLevelPracticeActivity.class));
            }
        });


        return view;
    }
}
