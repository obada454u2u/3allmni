package com.example.a3allmni.Practices;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a3allmni.DataProviders.lettersProvider;
import com.example.a3allmni.R;
import com.example.a3allmni.model.letter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class FourthLevelPracticeActvity extends AppCompatActivity implements View.OnClickListener {

    private letter[] DATA;
    private ImageView iv;
    private TextView tv0, tv1, tv2, tv3, tv4, tv5, tv6, tv7;
    int Findex, Sindex, Thindex, fIndex, fiIndex, sixIndex, seIndex, eIndex;
    private letter rightAnswer;
    char[] rightChars;
    String rightWord;
    char userChooice;
    char[] userChooices;
    int helper = 0;
    int helperRight = 0;
    int randomNumber, randomNumberImage;
    Toast mToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth_level_practice_actvity);

        DATA = lettersProvider.getLetters();

        iv = findViewById(R.id.practice_fourthlevel_iv);
        tv0 = findViewById(R.id.practice_fourthlevel_tv0);
        tv1 = findViewById(R.id.practice_fourthlevel_tv1);
        tv2 = findViewById(R.id.practice_fourthlevel_tv2);
        tv3 = findViewById(R.id.practice_fourthlevel_tv3);
        tv4 = findViewById(R.id.practice_fourthlevel_tv4);
        tv5 = findViewById(R.id.practice_fourthlevel_tv5);
        tv6 = findViewById(R.id.practice_fourthlevel_tv6);
        tv7 = findViewById(R.id.practice_fourthlevel_tv7);


        tv0.setOnClickListener(this);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
        tv4.setOnClickListener(this);
        tv5.setOnClickListener(this);
        tv6.setOnClickListener(this);
        tv7.setOnClickListener(this);


        geranteNewQuestion();

    }

    public void geranteNewQuestion() {
        Random random = new Random();

        randomNumber = random.nextInt(28); // 0 -> 27
        randomNumberImage = random.nextInt(2); // 0 -> 27

        rightAnswer = DATA[randomNumber];

        rightWord = rightAnswer.getWords().get(randomNumberImage).getTitle();
        iv.setBackgroundResource(rightAnswer.getWords().get(randomNumberImage).getImage());


        rightChars = new char[rightWord.length()];
        for (int i = 0; i < rightWord.length(); i++) {
            rightChars[i] = rightWord.charAt(i);
        }
        char[] allCahrs = new char[8];
        for (int i = 0; i < rightChars.length; i++)
            allCahrs[i] = rightChars[i];

        if (rightChars.length != 8) {
            for (int i = 0; i < 8 - rightChars.length; i++) {
                allCahrs[rightChars.length + i] = DATA[random.nextInt(28)].getTitle().charAt(0);
            }
        }

        generateRandomIndexes();
        tv0.setText(String.valueOf(allCahrs[Findex]));
        tv1.setText(String.valueOf(allCahrs[Sindex]));
        tv2.setText(String.valueOf(allCahrs[Thindex]));
        tv3.setText(String.valueOf(allCahrs[fIndex]));
        tv4.setText(String.valueOf(allCahrs[fiIndex]));
        tv5.setText(String.valueOf(allCahrs[sixIndex]));
        tv6.setText(String.valueOf(allCahrs[seIndex]));
        tv7.setText(String.valueOf(allCahrs[eIndex]));

        tv0.setBackgroundResource(R.drawable.rounded_textview);
        tv1.setBackgroundResource(R.drawable.rounded_textview);
        tv2.setBackgroundResource(R.drawable.rounded_textview);
        tv3.setBackgroundResource(R.drawable.rounded_textview);
        tv4.setBackgroundResource(R.drawable.rounded_textview);
        tv5.setBackgroundResource(R.drawable.rounded_textview);
        tv6.setBackgroundResource(R.drawable.rounded_textview);
        tv7.setBackgroundResource(R.drawable.rounded_textview);

        userChooices = new char[8];
        helperRight = 0;
        helper = 0;
    }


    public void generateRandomIndexes() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        Findex = list.get(0);
        Sindex = list.get(1);
        Thindex = list.get(2);
        fIndex = list.get(3);
        fiIndex = list.get(4);
        sixIndex = list.get(5);
        seIndex = list.get(6);
        eIndex = list.get(7);
    }


    public void showAToast(String message) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        TextView tv = (TextView) v;


        if (helperRight != rightChars.length && helper < 8) {
            userChooice = tv.getText().charAt(0);

            userChooices[helper] = userChooice;

            if (isCharInArray(userChooice)) {
                helper++;
                helperRight++;
                tv.setBackgroundResource(R.drawable.right_rounded_textview);
                tv.setText("✅");

                if (helperRight == rightChars.length) {
                    geranteNewQuestion();
                    showAToast(" Bravo ... ");
                }
            } else {

                if(tv.getText()!="✅")
                    showAToast(" Wrong !!");
            }
        }

    }

    public boolean isCharInArray(char a) {

        for (char rightChar : rightChars) {
            if (a == rightChar) {
                return true;
            }
        }
        return false;
    }

}
