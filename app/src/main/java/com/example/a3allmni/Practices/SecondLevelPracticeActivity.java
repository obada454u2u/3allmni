package com.example.a3allmni.Practices;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a3allmni.DataProviders.lettersProvider;

import com.example.a3allmni.R;
import com.example.a3allmni.SingleLetterActivity;
import com.example.a3allmni.model.letter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SecondLevelPracticeActivity extends AppCompatActivity implements View.OnClickListener {
    private letter[] DATA;
    private ImageView iv;
    Button btn1,btn2,btn3;
    int Findex,Sindex,Thindex;
    private letter rightAnswer;
    Toast mToast;
    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_level_practice);
        DATA= lettersProvider.getLetters();
        iv= findViewById(R.id.practice_secondlevel_iv);
        btn1= findViewById(R.id.practice_secondlevel_1btn);
        btn2= findViewById(R.id.practice_secondlevel_2btn);
        btn3= findViewById(R.id.practice_secondlevel_3btn);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        geranteNewQuestion();
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null ) {
                    if(mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                        mediaPlayer.release();
                    }
                }
                mediaPlayer = MediaPlayer.create(SecondLevelPracticeActivity.this,rightAnswer.getPronounce());
                mediaPlayer.start();
            }
        });

    }


    public void geranteNewQuestion(){
        Random random = new Random();

        int randomNumber=  random.nextInt(28); // 0 -> 27

        rightAnswer = DATA[randomNumber];



        final List<letter> possible = new ArrayList<>();

        possible.add(DATA[randomNumber]);
        possible.add(DATA[random.nextInt(28)]);
        possible.add(DATA[random.nextInt(28)]);

        genrateRandomIndexes();
        btn1.setText(possible.get(Findex).getTitle());
        btn2.setText(possible.get(Sindex).getTitle());
        btn3.setText(possible.get(Thindex).getTitle());
    }


    public void genrateRandomIndexes(){
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0; i<3; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        Findex = list.get(0);
        Sindex = list.get(1);
        Thindex = list.get(2);
    }


    public void showAToast (String message){
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        mToast.show();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if(mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer = null;
        }
    }

    @Override
    public void onClick(View v) {
        Button b = (Button) v;
        if (rightAnswer.getTitle()==b.getText()){
            showAToast("أصبت !");
            geranteNewQuestion();
        }else{
            showAToast("جرب مرة أخرى !!!!");
            geranteNewQuestion();
        }
    }
}
