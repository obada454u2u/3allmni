package com.example.a3allmni;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ShortVowelActivity extends AppCompatActivity {

    ImageView iv1,iv2,iv3 ;
    TextView tv1,tv2,tv3,subtv1,subtv2,subtv3,maintv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_short_vowel);


            findViews();


    }



    public void findViews(){
        maintv=findViewById(R.id.short_tv);
        tv1=findViewById(R.id.short_firsttv);
        tv2=findViewById(R.id.short_secondtv);
        tv3=findViewById(R.id.short_thirdtv);
        subtv1=findViewById(R.id.short_subtitle_first_tv);
        subtv2=findViewById(R.id._short_subtitile_Secondtv);
        subtv3=findViewById(R.id.short_subtitle_thirdtv);
        iv1=findViewById(R.id.short_firstIV);
        iv2=findViewById(R.id.short_secondIV);
        iv3=findViewById(R.id.short_thirdiv);
    }
}
