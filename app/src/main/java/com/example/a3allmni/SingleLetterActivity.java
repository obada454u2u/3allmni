package com.example.a3allmni;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.a3allmni.DataProviders.lettersProvider;
import com.example.a3allmni.model.letter;

public class SingleLetterActivity extends AppCompatActivity {


    TextView maintv,tv1,tv2;
    ImageView  iv1,iv2;
    MediaPlayer mediaPlayer;
   letter[] letter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_letter);
         letter= lettersProvider.getLetters();

     //   imageView= findViewById(R.id.title_im);
        iv1= findViewById(R.id.firstIV);
        iv2= findViewById(R.id.secondIV);
        tv1=findViewById(R.id.firsttv);
        tv2=findViewById(R.id.Secondtv);
        maintv=findViewById(R.id.title_tv);


      final  int CurrentPostion = getIntent().getIntExtra("letter",1);


       maintv.setText(letter[CurrentPostion].getTitle());
       iv1.setBackgroundResource(letter[CurrentPostion].getWords().get(0).getImage());
       iv2.setBackgroundResource(letter[CurrentPostion].getWords().get(1).getImage());
       tv1.setText(letter[CurrentPostion].getWords().get(0).getTitle());
       tv2.setText(letter[CurrentPostion].getWords().get(1).getTitle());


        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SingleLetterActivity.this,SingleWordActivity.class);
                i.putExtra("IMAGE_ID",letter[CurrentPostion].getWords().get(0).getImage());
                i.putExtra("IMAGE_TITLE",letter[CurrentPostion].getWords().get(0).getTitle());
                startActivity(i);
            }
        });
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SingleLetterActivity.this,SingleWordActivity.class);
                i.putExtra("IMAGE_ID",letter[CurrentPostion].getWords().get(1).getImage());
                i.putExtra("IMAGE_TITLE",letter[CurrentPostion].getWords().get(1).getTitle());
                startActivity(i);

            }
        });

        maintv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null ) {
                    if(mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                        mediaPlayer.release();
                    }
                }

                mediaPlayer = MediaPlayer.create(SingleLetterActivity.this,letter[CurrentPostion].getPronounce());
                mediaPlayer.start();
            }
        });

        }


    @Override
    protected void onStop() {
        super.onStop();
        if(mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer = null;
        }
    }
}