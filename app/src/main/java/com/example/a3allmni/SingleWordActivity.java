package com.example.a3allmni;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class SingleWordActivity extends AppCompatActivity {
    public static final String TAG = "SingleWordActivity";
    ImageView iv;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_word);
        iv=findViewById(R.id.signleword_iv);
        tv=findViewById(R.id.signleword_tv);

        int IMAGE_ID = getIntent().getIntExtra("IMAGE_ID",0);
        String IMAGE_TITLE = getIntent().getStringExtra("IMAGE_TITLE");

        iv.setBackgroundResource( IMAGE_ID);
        tv.setText(IMAGE_TITLE);

    }
}
