package com.example.a3allmni.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Word implements Parcelable {


    private String title;
    private int image;

    public Word( String title, int image) {


        this.title = title;
        this.image = image;
    }


    protected Word(Parcel in) {
        title = in.readString();
        image = in.readInt();
    }

    public static final Creator<Word> CREATOR = new Creator<Word>() {
        @Override
        public Word createFromParcel(Parcel in) {
            return new Word(in);
        }

        @Override
        public Word[] newArray(int size) {
            return new Word[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(image);
    }
}
