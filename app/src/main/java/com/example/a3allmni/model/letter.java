package com.example.a3allmni.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Map;

@SuppressLint("ParcelCreator")
public class letter implements Parcelable {

    private int id;
    private String title;
    private int image;
    private int pronounce;
    List<Word> words;


    public letter(int id, String title, int image, int pronounce, List<Word> word) {

        this.words = word;
        this.id = id;
        this.title = title;
        this.image = image;
        this.pronounce = pronounce;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getPronounce() {
        return pronounce;
    }

    public void setPronounce(int pronounce) {
        this.pronounce = pronounce;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}